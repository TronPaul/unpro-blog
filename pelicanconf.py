#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

THEME = 'unpro-blueidea'
AUTHOR = u'Mark McGuire'
SITENAME = u'UnPRO'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/Chicago'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
DISPLAY_CATEGORIES_ON_SUBMENU = True
DISPLAY_CATEGORIES_ON_MENU = False

# Blogroll
#LINKS = (('Pelican', 'http://getpelican.com/'),
#         ('Python.org', 'http://python.org/'),
#         ('Jinja2', 'http://jinja.pocoo.org/'),
#         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('Github Profile', 'https://github.com/TronPaul'),
          ('Twitter', 'https://twitter.com/tronpaul'),
          ('Steam Profile', 'https://steamcommunity.com/id/TronPaul'),
          ('Twitch.tv', 'http://twitch.tv/tronpaul'),)

FOOTER = 'The views and opinions expressed in this blog are solely my own and are not associated with my employers past, present or future'

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
