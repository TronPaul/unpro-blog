My Development Principles
#########################

:date: 2016-12-26 16:53
:category: Development
:tags: development
:slug: development-principles-1
:summary: A few of my development principles

Recently I ran into a poor API implemetation while trying to consume said API.
The API would return a 200 response which included a JSON encoded HTTP 407 error.
Obviously this is wrong. The API is a lying liar. I laughed to myself and thought,
"Note to self, don't do this." And got back to work. Shortly after though I wondered
what other notes and opinions did I have about software. I decided to try and come
up with as many that I try to follow as I could as a thought experiment.

Below is the current version of `Development Principles </pages/development-principles>`_.
I plan on updating it periodically as I develop new thoughts and opinions in this
Wild West we call Software Design and Development.

Development Principles
======================

This is a collection of opinions I have about software development and design,
written as more of a thought experiment than bible or manifesto. They are not
in any particular order. Don't let dogma get in the way of getting things done,
and there's no dogma like no dogma.


Don't lie: 
  Return correct HTTP error codes, throw exceptions when an operation isn't
  recoverable, etc.

Avoid unneeded verbosity:
  Don't repeat yourself in comments, use language patterns where available to be
  concise.

Don't abstract prematurely:
  This is a premature optimization. You will end up being tightly coupled to
  something at some point. Bad abstractions are worse than coupling.

Test coverage lies:
  Don't blindly seek coverage. Evaluate what to cover and how to cover it. You will
  miss failure cases, but try and eliminate the dangerous ones.

Re-evaluate often:
  If something is taking months to accomplish you might be using the wrong tool, or
  trying to solve the wrong problem.

Get to the root of requirements:
  Acceptance criteria shouldn't tell you to use a database. You are paid to find
  solutions, not to blindly implement someone's requirements.

Convention over personal preference:
  Follow shared conventions as much as possible. Challenge conventions when you have
  a disagreement, but accept the team's decision or move on.

Caching is dangerous:
  Perfer fixing the upstream, over adding an external caching service, over adding
  caching to the application.

Avoid clverness:
  Cleverness should be reserved for hard or difficult problems. The simplest solution
  is usually the best solution.

Don't write DSLs:
  YAML + a templating engine should be all you need. Subset of avoiding cleverness.

Don't commit commented out code:
  You have a VCS. Use it.

Avoid TODOs:
  Use a ticketing system if you have one.

Make the code readable:
  Avoid nested branching, long functionso, large classes. Follow style conventions and
  use good naming. See Robert Martin's Clean Code for a starting point. It's tightly
  coupled to Java conventions, but still helpful.
