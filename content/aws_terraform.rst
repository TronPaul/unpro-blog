Adventures in AWS: Switching to Terraform
#########################################

:date: 2016-11-12 15:09
:category: Projects
:tags: aws, operations
:slug: adventures-in-aws-terraform
:summary: Using Terraform in AWS

Deciding to Switch
==================

Previously I was using a custom application to convert yaml files into
Cloudformation templates. This worked, but I was stuck using Cloudformation
which wasn't very good at updates and changes. Plus since it was custom,
if anything changed I had to write it.

Given that I hadn't done a lot of operations work on my servers it made
sense to switch to something that was maintained by somebody else.

Thoughts on Terraform
=====================
I hate custom DSLs. People need to stop writing them. Your data can be
described by YAML, it's easier for other programs to generate and
manipulate, and just about everyone knows it.

Besides that Terraform is nice. The information about what will be done
and why is easy to get at and understand. The output while bring up
resources is good, and errors don't fully rollback your stack.

The one other thing I ran into trouble with was EC2 user data, templating,
and resource variables. Templates can't access the properties of the
resource using them. That means I have to duplicate the names of instances
in places rather than just pulling a field off of the resource.

What's Next
===========

I want to try out Lambda and ECS a bit more than I have. I think Lambda is
a interesting idea and have a couple of ideas on how to use it. I'd like to
auto-deploy this blog for example, or maybe even deploy a Flask app to it.
The one big issue is no Python 3.

I've tried ECS a bit already and I've been frustrated by it. I don't like
the fact I have to manage the cluster nodes myself. I just want to be
charged for how much my containers are using and not have to worry about
spinning up and down nodes. ECS's task management is also a bit clunky
and makes me wonder if I shouldn't just try Mesos instead.
